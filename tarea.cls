\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{tarea}

\DeclareOption*{\PassOptionsToClass{\CurrentOption}{article}}
\ProcessOptions \relax
\LoadClass{article}

\RequirePackage[letterpaper,left=2cm,right=2cm,top=2cm,bottom=2cm]{geometry}
\RequirePackage[utf8]{inputenc}
\RequirePackage[spanish, es-tabla,es-lcroman]{babel}
\RequirePackage{subfiles}
\RequirePackage{amsmath}
\RequirePackage{amsfonts}
\RequirePackage{amssymb}
\RequirePackage{mathrsfs}
\RequirePackage{graphicx}
\RequirePackage{enumerate}
\RequirePackage{titlesec}
\RequirePackage{fancyhdr}
\RequirePackage{multicol}
\RequirePackage{caption}
\RequirePackage{tabularx}

\titleformat*{\section}{\bfseries}
\titleformat*{\subsection}{\bfseries}
\titleformat*{\subsubsection}
%\titlelabel{\thetitle. \,}

\newcommand\curso[1]{\renewcommand\@curso{#1}}
\newcommand\@curso{}

\newcommand\ntarea[1]{\renewcommand\@ntarea{ #1}}
\newcommand\@ntarea{}

\newcommand\tarea[1]{\renewcommand\@tarea{#1}}
\newcommand\@tarea{Tarea}

\newcommand\profesor[1]{\renewcommand\@profesor{
    #1
  }}
\newcommand\@profesor{}

\newcommand\ayudante[1]{\renewcommand\@ayudante{
    #1     
  }}
\newcommand\@ayudante{}

\thispagestyle{fancy}
\fancyh{}
\lhead{
  \begin{tabular}[t]{l}
    \small{Facultad de Ciencias}\\
    \small{Universidad de Chile}
  \end{tabular}
}
\fancyhead[R]{
  \small{\begin{tabular}[t]{rl}
           Profesor:\hspace{-4mm}
           &
             \begin{tabular}[t]{l}
               \@profesor
             \end{tabular}\\
           Ayudante:\hspace{-4mm}
           &
             \begin{tabular}[t]{l}
               \@ayudante
             \end{tabular}
         \end{tabular}}
}

\setlength{\headheight}{25pt}
\renewcommand{\headrulewidth}{0.0pt}
\fancyheadoffset[l]{6pt}
\fancyheadoffset[r]{15pt}
      
\def\maketitle{
  \begin{center}
    \textbf{
      \Large{\@tarea\space\@ntarea}
      \vspace{3pt}
      \large{\\\@curso}
    }
  \end{center}
  \vspace{15pt}

\noindent \textbf{\@author} \hfill \textbf{\@date}\\

}

\endinput
